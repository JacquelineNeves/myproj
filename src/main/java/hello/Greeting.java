package hello;

import java.util.Calendar;
import java.util.Random;

public class Greeting {
    Random random = new Random();
    Calendar calendar = Calendar.getInstance();
    private String name = "Jacqueline Neves";
    private int numero = random.nextInt(101);

    private int dia = calendar.get(Calendar.DAY_OF_MONTH);
    private int hora = calendar.get(Calendar.HOUR_OF_DAY);
    private int minuto = calendar.get(Calendar.MINUTE);
    private int segundo = calendar.get(Calendar.SECOND);

    private String dias = "Dia:" + dia + "Horas:" + hora + ":" + minuto + ":" + segundo;

    public int getDia() {
        return dia;
    }

    public int getHora() {
        return hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public int getSegundo() {
        return segundo;
    }

    public String getName() {
        return name;
    }

    public int getNumero() {
        return numero;
    }
}